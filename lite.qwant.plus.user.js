// ==UserScript==
// @name        Qwant lite enhancer
// @description replace redirects with actual links
// @include     https://lite.qwant.com/*
// @icon        https://lite.qwant.com/favicon.ico
// ==/UserScript==

document.querySelectorAll("div.result div.title").forEach( el => {
    var url = decodeURIComponent(el.firstChild.getAttribute('href')).split('=/').pop().split('?position')[0];
    if (url.startsWith("http://")) {
        el.parentElement.setAttribute('style', 'background-color: orange;');
    }
    el.firstChild.setAttribute('href', url);
});
